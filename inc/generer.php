﻿<?php

session_start();

$env = '../.env';
if (isset($_SESSION['domainesAutorises']) || file_exists($env)) {
	if (isset($_SESSION['domainesAutorises']) && $_SESSION['domainesAutorises'] !== '') {
		$domainesAutorises = $_SESSION['domainesAutorises'];
	} else if (file_exists($env)) {
		$donneesEnv = explode("\n", file_get_contents($env));
		foreach ($donneesEnv as $ligne) {
			preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
			if (isset($matches[2])) {
				putenv(trim($ligne));
			}
		}
		$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
		$_SESSION['domainesAutorises'] = $domainesAutorises;
	}
	if ($domainesAutorises === '*') {
		$origine = $domainesAutorises;
	} else {
		$domainesAutorises = explode(',', $domainesAutorises);
		$origine = $_SERVER['SERVER_NAME'];
	}
	if ($origine === '*' || in_array($origine, $domainesAutorises, true)) {
		header('Access-Control-Allow-Origin: $origine');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
	} else {
		header('Location: ../');
		exit();
	}
} else {
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Max-Age: 1000');
	header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
}

if (!empty($_FILES['fichier']) && !empty($_POST['domaine']) && !empty($_POST['question']) && !empty($_POST['reponse'])) {
	require 'db.php';
	$id = uniqid('', false);
	if (is_dir('../q/' . $id) === false) {
		$temp = '../q/' . $id . '/temp';
		$h5p = '../q/' . $id . '/h5p';
		mkdir('../q/' . $id, 0775, true);
		mkdir($h5p, 0775, true);
		mkdir($temp, 0775, true);
		$fichier = $h5p . '/' . basename($_FILES['fichier']['name']);
		if (move_uploaded_file($_FILES['fichier']['tmp_name'], $fichier)) {
			$domaine = $_POST['domaine'];
			$question = $_POST['question'];
			$reponse = password_hash(strtolower($_POST['reponse']), PASSWORD_DEFAULT);
			$date = date('Y-m-d H:i:s');
			$stmt = $db->prepare('INSERT INTO digiquiz_contenus (url, question, reponse, date) VALUES (:url, :question, :reponse, :date)');
			if ($stmt->execute(array('url' => $id, 'question' => $question, 'reponse' => $reponse, 'date' => $date))) {
				$zip = new ZipArchive;
				if ($zip->open($fichier) === TRUE) {
					$zip->extractTo($temp);
					$zip->close();
					rename($temp . '/h5p.json', $h5p . '/h5p.json');
					rename($temp . '/content', $h5p . '/content');
					$h5pJson = file_get_contents($h5p . '/h5p.json');
					$json = json_decode($h5pJson, true);
					array_push($json['preloadedDependencies'], array('machineName' => 'H5P.MathDisplay', 'majorVersion' => 1, 'minorVersion' => 0));
					$titre = $json['title'];
					file_put_contents($h5p . '/h5p.json', json_encode($json));
					$html = '<!DOCTYPE html>
					<html lang="fr">
						<head>
							<meta charset="utf-8" />
							<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5, minimal-ui, viewport-fit=cover" />
							<meta name="keywords" content="digital learning, blended learning, h5p, language teaching tools, edtech, classe virtuelle, éducation" />
							<meta name="description" content="Un contenu interactif réalisé avec H5P et diffusé par Digiquiz by La Digitale" />
							<meta name="robots" content="index, no-follow" />
							<meta name="theme-color" content="#001d1d" />
							<meta name="format-detection" content="telephone=no" />
							<meta name="msapplication-tap-highlight" content="no" />
							<meta name="apple-mobile-web-app-capable" content="yes" />
							<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
							<meta name="HandheldFriendly" content="true" />
							<meta property="og:title" content="' . $titre . ' - Digiquiz by La Digitale" />
							<meta property="og:description" content="Un contenu interactif réalisé avec H5P et diffusé par Digiquiz by La Digitale" />
							<meta property="og:type" content="website" />
							<meta property="og:url" content="' . $domaine . '" />
							<meta property="og:locale" content="fr_FR" />
							<title>' . $titre . ' - Digiquiz by La Digitale</title>
							<link rel="stylesheet" href="../../styles/destyle.css" />
							<link rel="stylesheet" href="../../styles/style.css" />
							<link rel="stylesheet" href="../../styles/h5p.css" />
							<link rel="apple-touch-icon" href="../../img/apple-touch-icon.png" />
							<link rel="icon" href="../../img/favicon.png" />
						</head>
						<body>
							<main>
								<div id="conteneur-chargement">
									<div id="chargement">
										<div class="spinner">
											<div></div>
											<div></div>
											<div></div>
											<div></div>
											<div></div>
											<div></div>
											<div></div>
											<div></div>
											<div></div>
											<div></div>
											<div></div>
											<div></div>
										</div>
									</div>
								</div>
								<header>
									<div id="conteneur-header">
										<div class="conteneur">
											<a href="' . $domaine . '"><span id="logo"></span><span id="titre">Digiquiz by La Digitale</span></a>
										</div>
										<div class="conteneur">
											<span id="partager" title="Partager">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px">
													<path d="M0 0h24v24H0z" fill="none" />
													<path
														d="M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z"
													/>
												</svg>
											</span>
											<span id="parametres" title="Paramètres">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" height="24px" width="24px">
													<path d="M0,0h24v24H0V0z" fill="none"/>
													<path 
														d="M19.14,12.94c0.04-0.3,0.06-0.61,0.06-0.94c0-0.32-0.02-0.64-0.07-0.94l2.03-1.58c0.18-0.14,0.23-0.41,0.12-0.61 l-1.92-3.32c-0.12-0.22-0.37-0.29-0.59-0.22l-2.39,0.96c-0.5-0.38-1.03-0.7-1.62-0.94L14.4,2.81c-0.04-0.24-0.24-0.41-0.48-0.41 h-3.84c-0.24,0-0.43,0.17-0.47,0.41L9.25,5.35C8.66,5.59,8.12,5.92,7.63,6.29L5.24,5.33c-0.22-0.08-0.47,0-0.59,0.22L2.74,8.87 C2.62,9.08,2.66,9.34,2.86,9.48l2.03,1.58C4.84,11.36,4.8,11.69,4.8,12s0.02,0.64,0.07,0.94l-2.03,1.58 c-0.18,0.14-0.23,0.41-0.12,0.61l1.92,3.32c0.12,0.22,0.37,0.29,0.59,0.22l2.39-0.96c0.5,0.38,1.03,0.7,1.62,0.94l0.36,2.54 c0.05,0.24,0.24,0.41,0.48,0.41h3.84c0.24,0,0.44-0.17,0.47-0.41l0.36-2.54c0.59-0.24,1.13-0.56,1.62-0.94l2.39,0.96 c0.22,0.08,0.47,0,0.59-0.22l1.92-3.32c0.12-0.22,0.07-0.47-0.12-0.61L19.14,12.94z M12,15.6c-1.98,0-3.6-1.62-3.6-3.6 s1.62-3.6,3.6-3.6s3.6,1.62,3.6,3.6S13.98,15.6,12,15.6z"
													/>
												</svg>
											</span>
										</div>
									</div>
								</header>
								<div id="h5p"></div>
								<div id="menu-partager" class="menu" role="menu" tabindex="-1">
									<div id="conteneur-partager">
										<label>Lien et code QR :</label>
										<div id="copier-lien" class="copier">
											<input type="text" value="' . $domaine . 'q/' . $id . '" readonly />
											<span class="icone lien" role="button" tabindex="0" title="Copier le lien">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px">
													<path d="M0 0h24v24H0z" fill="none" />
													<path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z" />
												</svg>
											</span>
											<span class="icone codeqr" role="button" tabindex="0" title="Afficher le code QR">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px">
													<g><rect fill="none" height="24" width="24"/></g><g><g><path d="M3,11h8V3H3V11z M5,5h4v4H5V5z"/><path d="M3,21h8v-8H3V21z M5,15h4v4H5V15z"/><path d="M13,3v8h8V3H13z M19,9h-4V5h4V9z"/><rect height="2" width="2" x="19" y="19"/><rect height="2" width="2" x="13" y="13"/><rect height="2" width="2" x="15" y="15"/><rect height="2" width="2" x="13" y="17"/><rect height="2" width="2" x="15" y="19"/><rect height="2" width="2" x="17" y="17"/><rect height="2" width="2" x="17" y="13"/><rect height="2" width="2" x="19" y="15"/></g></g>
												</svg>
											</span>
										</div>
										<label>Code d&apos;intégration :</label>
										<div id="copier-iframe" class="copier">
											<input type="text" value="<div style=&quot;width:100%; height:500px; overflow:hidden&quot;><iframe allowfullscreen src=&quot;' . $domaine . 'q/' . $id . '/&quot; frameborder=&quot;0&quot; width=&quot;100%&quot; height=&quot;500&quot; style=&quot;overflow:hidden; background:#fff&quot;></iframe></div>" readonly />
											<span class="icone" role="button" tabindex="0" title="Copier le code d&apos;intégration">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px">
													<path d="M0 0h24v24H0z" fill="none" />
													<path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z" />
												</svg>
											</span>
										</div>
									</div>
								</div>
								<div id="modale-parametres" class="conteneur-modale" role="dialog" tabindex="-1">
									<div class="modale" role="document">
										<header>
											<span class="titre">Paramètres</span>
											<span id="fermer" class="fermer" role="button" tabindex="0">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></svg>
											</span>
										</header>
										<div class="conteneur">
											<div class="contenu">
												<label>Question secrète</label>
												<select>
													<option selected>Quel est mon mot préféré ?</option>
													<option>Quel est mon film préféré ?</option>
													<option>Quelle est ma chanson préférée ?</option>
													<option>Quel est le prénom de ma mère ?</option>
													<option>Quel est le prénom de mon père ?</option>
													<option>Quel est le nom de ma rue ?</option>
													<option>Quel est le nom de mon employeur ?</option>
													<option>Quel est le nom de mon animal de compagnie ?</option>
												</select>
												<label>Réponse secrète</label>
												<input type="password">
												<div class="actions">
													<span id="supprimer" class="bouton" role="button" tabindex="0">Supprimer</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="modale-codeqr" class="conteneur-modale" role="dialog" tabindex="-1">
									<div class="modale" role="document">
										<header>
											<span class="titre">Code QR</span>
											<span id="fermerQR" class="fermer" role="button" tabindex="0">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></svg>
											</span>
										</header>
										<div class="conteneur">
											<div class="contenu">
												<div id="qr"></div>
											</div>
										</div>
									</div>
								</div>
							</main>
							<script src="../../scripts/clipboard.js" type="text/javascript"></script>
							<script src="../../scripts/qrcode.js" type="text/javascript"></script>
							<script src="../../scripts/main.bundle.js" type="text/javascript"></script>
							<script type="text/javascript">
								const el = document.querySelector("#h5p");
								const options = {
									h5pJsonPath: "./h5p",
									frameJs: "../../scripts/frame.bundle.js",
									frameCss: "../../styles/h5p.css",
									librariesPath: "../../libraries",
									frame: false,
									copyright: true,
									embed: false,
									download: false,
									icon: false,
									export: false,
									fullScreen: true,
									reportingIsEnabled: false,
									customCss: ["../../styles/h5p-confirmation-dialog.css", "../../styles/h5p-core-button.css"]
								};
								window.addEventListener("load", function () {
									const h5p = new H5PStandalone.H5P(el, options);
									h5p.then(() => {
										setTimeout(function () {
											document.querySelector("#conteneur-chargement").style.display = "none";
										}, 300);
									});
								});
								document.querySelector("#partager").addEventListener("click", function (event) {
									if (document.querySelector("#menu-partager").classList.contains("active")) {
										document.querySelector("#menu-partager").classList.remove("active");
									} else {
										document.querySelector("#menu-partager").classList.add("active");
										const largeurBouton = event.target.getBoundingClientRect().width;
										const largeurMenu = document.querySelector("#menu-partager").getBoundingClientRect().width;
										const position = event.target.getBoundingClientRect().left - ((largeurMenu * 90) / 100 - largeurBouton / 2);
										document.querySelector("#menu-partager").style.left = position + "px";
									}
								});
								const clipboard = new ClipboardJS("#copier-lien .lien", {
									container: document.querySelector("#menu-partager"),
									text: function () {
										return "' . $domaine . 'q/' . $id . '";
									},
								});
								clipboard.on("success", function () {
									const element = document.createElement("div");
									const id = "notification_" + Date.now().toString(36) + Math.random().toString(36).substr(2);
									element.id = id;
									element.innerHTML = "Lien copié dans le presse-papier.";
									element.classList.add("notification");
									document.body.appendChild(element);
									setTimeout(function () {
										element.parentNode.removeChild(element);
									}, 2000);
								});
								const clipboardIframe = new ClipboardJS("#copier-iframe span", {
									container: document.querySelector("#menu-partager"),
									text: function () {
										return document.querySelector("#copier-iframe input").value;
									},
								});
								clipboardIframe.on("success", function () {
									const element = document.createElement("div");
									const id = "notification_" + Date.now().toString(36) + Math.random().toString(36).substr(2);
									element.id = id;
									element.innerHTML = "Code d&apos;intégration copié dans le presse-papier.";
									element.classList.add("notification");
									document.body.appendChild(element);
									setTimeout(function () {
										element.parentNode.removeChild(element);
									}, 2000);
								});
								new QRCode("qr", {
									text: "' . $domaine . 'q/' . $id . '",
									width: 360,
									height: 360,
									colorDark: "#000000",
									colorLight: "#ffffff",
									correctLevel : QRCode.CorrectLevel.H
								});
								document.addEventListener("click", function (event) {
									const partager = document.querySelector("#partager");
									const menuPartager = document.querySelector("#menu-partager");
									if (menuPartager && event.target !== partager && event.target !== menuPartager && !partager.contains(event.target) && !menuPartager.contains(event.target)) {
										document.querySelector("#menu-partager").classList.remove("active");
									}
								});
								document.querySelector("#parametres").addEventListener("click", function (event) {
									document.querySelector("#modale-parametres").classList.add("ouvert");
								});
								document.querySelector("#fermer").addEventListener("click", function () {
									document.querySelector("#modale-parametres").classList.remove("ouvert");
									document.querySelector("#modale-parametres input").value = "";
								});
								document.querySelector("#copier-lien .codeqr").addEventListener("click", function (event) {
									document.querySelector("#menu-partager").classList.remove("active");
									document.querySelector("#modale-codeqr").classList.add("ouvert");
								});
								document.querySelector("#fermerQR").addEventListener("click", function () {
									document.querySelector("#modale-codeqr").classList.remove("ouvert");
								});
								document.querySelector("#supprimer").addEventListener("click", function () {
									const select = document.querySelector("#modale-parametres select");
									const question = select.options[select.selectedIndex].text
									const reponse = document.querySelector("#modale-parametres input").value;
									if (reponse !== "") {
										document.querySelector("#conteneur-chargement").style.display = "block";
										const xhr = new XMLHttpRequest();
										xhr.onload = function () {
											let donnees
											if (xhr.readyState === xhr.DONE && xhr.status === 200) {
												if (xhr.responseText === "erreur") {
													document.querySelector("#conteneur-chargement").style.display = "none";
													alert("Erreur lors de la suppression du contenu.");
												} else if (xhr.responseText === "non_autorise") {
													document.querySelector("#conteneur-chargement").style.display = "none";
													alert("Les informations indiquées ne sont pas correctes.");
												} else {
													window.location = "' . $domaine . '";
												}
											} else {
												document.querySelector("#conteneur-chargement").style.display = "none";
												alert("Erreur lors de la suppression du contenu.");
											}
										};
										xhr.open("POST", "../../inc/supprimer.php", true);
										xhr.setRequestHeader("Content-type", "application/json");
										xhr.send(JSON.stringify({ id: "' . $id . '", question: question, reponse: reponse }));
									} else {
										alert("Veuillez complétez les champs du formulaire.");
									}
								});
								if (window !== window.parent) {
									document.querySelector("header").style.display = "none";
									document.querySelector("#h5p").style.margin = "0 auto";
									document.querySelector("#h5p").style.height = "100%";
								}
							</script>
						</body>
					</html>';
					file_put_contents('../q/' . $id . '/index.html', $html);
					unlink($fichier);
					supprimer($temp);
					echo $id;
				} else {
					echo 'erreur_extraction';
				}
			} else {
				echo 'erreur';
			}
		} else {
			echo 'erreur_televersement';
		}
	} else {
		echo 'contenu_existe_deja';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

function supprimer ($path) {
	if (is_dir($path) === true) {
		$files = array_diff(scandir($path), array('.', '..'));
		foreach ($files as $file) {
			supprimer(realpath($path) . '/' . $file);
		}
		return rmdir($path);
	} else if (is_file($path) === true) {
		return unlink($path);
	}
	return false;
}

?>
