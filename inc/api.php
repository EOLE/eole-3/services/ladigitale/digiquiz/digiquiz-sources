<?php

session_start();

if ($_SERVER['SERVER_NAME'] === 'localhost' || $_SERVER['SERVER_NAME'] === '127.0.0.1') {
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Max-Age: 1000');
	header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
} else {
	$env = '../.env';
	if (isset($_SESSION['domainesAutorises']) || file_exists($env)) {
		if (isset($_SESSION['domainesAutorises']) && $_SESSION['domainesAutorises'] !== '') {
			$domainesAutorises = $_SESSION['domainesAutorises'];
		} else if (file_exists($env)) {
			$donneesEnv = explode("\n", file_get_contents($env));
			foreach ($donneesEnv as $ligne) {
				preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
				if (isset($matches[2])) {
					putenv(trim($ligne));
				}
			}
			$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
			$_SESSION['domainesAutorises'] = $domainesAutorises;
		}
		if ($domainesAutorises === '*') {
			$origine = $domainesAutorises;
		} else {
			$domainesAutorises = explode(',', $domainesAutorises);
			$origine = $_SERVER['SERVER_NAME'];
		}
		if ($origine === '*' || in_array($origine, $domainesAutorises, true)) {
			header('Access-Control-Allow-Origin: $origine');
			header('Access-Control-Allow-Methods: POST');
			header('Access-Control-Max-Age: 1000');
			header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
		} else {
			echo 'erreur';
			exit();
		}
	} else {
		echo 'erreur';
		exit();
	}
}

if (!$_POST) {
	$_POST = json_decode(file_get_contents('php://input'), true);
}

if (!empty($_POST['token']) && !empty($_POST['lien'])) {
	$token = $_POST['token'];
	$domaine = $_SERVER['SERVER_NAME'];
	$lien = $_POST['lien'];
	$donnees = array(
		'token' => $token,
		'domaine' => $domaine
	);
	$donnees = http_build_query($donnees);
	$ch = curl_init($lien);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $donnees);
	$resultat = curl_exec($ch);
	if ($resultat === 'non_autorise' || $resultat === 'erreur') {
		echo 'erreur_token';
	} else if ($resultat === 'token_autorise' && !empty($_POST['action'])) {
		$action = $_POST['action'];
		if ($action === 'creer' && !empty($_POST['nom']) && !empty($_POST['question']) && !empty($_POST['reponse']) && !empty($_FILES['fichier']) && !empty($_POST['domaine'])) {
			require 'db.php';
			$id = uniqid('', false);
			$temp = '../q/' . $id . '/temp';
			$h5p = '../q/' . $id . '/h5p';
			mkdir('../q/' . $id, 0775, true);
			mkdir($h5p, 0775, true);
			mkdir($temp, 0775, true);
			$fichier = $h5p . '/' . basename($_FILES['fichier']['name']);
			if (move_uploaded_file($_FILES['fichier']['tmp_name'], $fichier)) {
				$digiquiz = $_POST['domaine'];
				$question = definirQuestion($_POST['question']);
				$reponse = password_hash(strtolower($_POST['reponse']), PASSWORD_DEFAULT);
				$nom = $_POST['nom'];
				$resultats = $_POST['resultats'];
				$date = date('Y-m-d H:i:s');
				$stmt = $db->prepare('INSERT INTO digiquiz_contenus (url, question, reponse, date) VALUES (:url, :question, :reponse, :date)');
				if ($stmt->execute(array('url' => $id, 'question' => $question, 'reponse' => $reponse, 'date' => $date))) {
					$zip = new ZipArchive;
					if ($zip->open($fichier) === TRUE) {
						$zip->extractTo($temp);
						$zip->close();
						rename($temp . '/h5p.json', $h5p . '/h5p.json');
						rename($temp . '/content', $h5p . '/content');
						$h5pJson = file_get_contents($h5p . '/h5p.json');
						$json = json_decode($h5pJson, true);
						array_push($json['preloadedDependencies'], array('machineName' => 'H5P.MathDisplay', 'majorVersion' => 1, 'minorVersion' => 0));
						file_put_contents($h5p . '/h5p.json', json_encode($json));
						$html = '<!DOCTYPE html>
						<html lang="fr">
							<head>
								<meta charset="utf-8" />
								<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5, minimal-ui, viewport-fit=cover" />
								<meta name="keywords" content="digital learning, blended learning, h5p, language teaching tools, edtech, classe virtuelle, éducation" />
								<meta name="description" content="Un contenu interactif réalisé avec H5P et diffusé par Digiquiz by La Digitale" />
								<meta name="robots" content="index, no-follow" />
								<meta name="theme-color" content="#001d1d" />
								<meta name="format-detection" content="telephone=no" />
								<meta name="msapplication-tap-highlight" content="no" />
								<meta name="apple-mobile-web-app-capable" content="yes" />
								<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
								<meta name="HandheldFriendly" content="true" />
								<meta property="og:title" content="' . $nom . ' - Digiquiz by La Digitale" />
								<meta property="og:description" content="Un contenu interactif réalisé avec H5P et diffusé par Digiquiz by La Digitale" />
								<meta property="og:type" content="website" />
								<meta property="og:url" content="' . $digiquiz . '" />
								<meta property="og:locale" content="fr_FR" />
								<title>' . $nom . ' - Digiquiz by La Digitale</title>
								<link rel="stylesheet" href="../../styles/destyle.css" />
								<link rel="stylesheet" href="../../styles/style.css" />
								<link rel="stylesheet" href="../../styles/h5p.css" />
								<link rel="apple-touch-icon" href="../../img/apple-touch-icon.png" />
								<link rel="icon" href="../../img/favicon.png" />
							</head>
							<body>
								<main>
									<div id="conteneur-chargement">
										<div id="chargement">
											<div class="spinner">
												<div></div>
												<div></div>
												<div></div>
												<div></div>
												<div></div>
												<div></div>
												<div></div>
												<div></div>
												<div></div>
												<div></div>
												<div></div>
												<div></div>
											</div>
										</div>
									</div>
									<header>
										<div id="conteneur-header">
											<div class="conteneur">
												<a href="' . $digiquiz . '"><span id="logo"></span><span id="titre">Digiquiz by La Digitale</span></a>
											</div>
											<div class="conteneur">
												<span id="partager" title="Partager">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px">
														<path d="M0 0h24v24H0z" fill="none" />
														<path
															d="M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z"
														/>
													</svg>
												</span>
												<span id="parametres" title="Paramètres">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" height="24px" width="24px">
														<path d="M0,0h24v24H0V0z" fill="none"/>
														<path 
															d="M19.14,12.94c0.04-0.3,0.06-0.61,0.06-0.94c0-0.32-0.02-0.64-0.07-0.94l2.03-1.58c0.18-0.14,0.23-0.41,0.12-0.61 l-1.92-3.32c-0.12-0.22-0.37-0.29-0.59-0.22l-2.39,0.96c-0.5-0.38-1.03-0.7-1.62-0.94L14.4,2.81c-0.04-0.24-0.24-0.41-0.48-0.41 h-3.84c-0.24,0-0.43,0.17-0.47,0.41L9.25,5.35C8.66,5.59,8.12,5.92,7.63,6.29L5.24,5.33c-0.22-0.08-0.47,0-0.59,0.22L2.74,8.87 C2.62,9.08,2.66,9.34,2.86,9.48l2.03,1.58C4.84,11.36,4.8,11.69,4.8,12s0.02,0.64,0.07,0.94l-2.03,1.58 c-0.18,0.14-0.23,0.41-0.12,0.61l1.92,3.32c0.12,0.22,0.37,0.29,0.59,0.22l2.39-0.96c0.5,0.38,1.03,0.7,1.62,0.94l0.36,2.54 c0.05,0.24,0.24,0.41,0.48,0.41h3.84c0.24,0,0.44-0.17,0.47-0.41l0.36-2.54c0.59-0.24,1.13-0.56,1.62-0.94l2.39,0.96 c0.22,0.08,0.47,0,0.59-0.22l1.92-3.32c0.12-0.22,0.07-0.47-0.12-0.61L19.14,12.94z M12,15.6c-1.98,0-3.6-1.62-3.6-3.6 s1.62-3.6,3.6-3.6s3.6,1.62,3.6,3.6S13.98,15.6,12,15.6z"
														/>
													</svg>
												</span>
											</div>
										</div>
									</header>
									<div id="h5p"></div>
									<div id="menu-partager" class="menu" role="menu" tabindex="-1">
										<div id="conteneur-partager">
											<label>Lien et code QR :</label>
											<div id="copier-lien" class="copier">
												<input type="text" value="' . $digiquiz . 'q/' . $id . '" readonly />
												<span class="icone lien" role="button" tabindex="0" title="Copier le lien">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px">
														<path d="M0 0h24v24H0z" fill="none" />
														<path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z" />
													</svg>
												</span>
												<span class="icone codeqr" role="button" tabindex="0" title="Afficher le code QR">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px">
														<g><rect fill="none" height="24" width="24"/></g><g><g><path d="M3,11h8V3H3V11z M5,5h4v4H5V5z"/><path d="M3,21h8v-8H3V21z M5,15h4v4H5V15z"/><path d="M13,3v8h8V3H13z M19,9h-4V5h4V9z"/><rect height="2" width="2" x="19" y="19"/><rect height="2" width="2" x="13" y="13"/><rect height="2" width="2" x="15" y="15"/><rect height="2" width="2" x="13" y="17"/><rect height="2" width="2" x="15" y="19"/><rect height="2" width="2" x="17" y="17"/><rect height="2" width="2" x="17" y="13"/><rect height="2" width="2" x="19" y="15"/></g></g>
													</svg>
												</span>
											</div>
											<label>Code d&apos;intégration :</label>
											<div id="copier-iframe" class="copier">
												<input type="text" value="<div style=&quot;width:100%; height:500px; overflow:hidden&quot;><iframe allowfullscreen src=&quot;' . $digiquiz . 'q/' . $id . '/&quot; frameborder=&quot;0&quot; width=&quot;100%&quot; height=&quot;500&quot; style=&quot;overflow:hidden; background:#fff&quot;></iframe></div>" readonly />
												<span class="icone" role="button" tabindex="0" title="Copier le code d&apos;intégration">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px">
														<path d="M0 0h24v24H0z" fill="none" />
														<path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z" />
													</svg>
												</span>
											</div>
										</div>
									</div>
									<div id="modale-parametres" class="conteneur-modale" role="dialog" tabindex="-1">
										<div class="modale" role="document">
											<header>
												<span class="titre">Paramètres</span>
												<span id="fermer" class="fermer" role="button" tabindex="0">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></svg>
												</span>
											</header>
											<div class="conteneur">
												<div class="contenu">
													<label>Question secrète</label>
													<select>
														<option selected>Quel est mon mot préféré ?</option>
														<option>Quel est mon film préféré ?</option>
														<option>Quelle est ma chanson préférée ?</option>
														<option>Quel est le prénom de ma mère ?</option>
														<option>Quel est le prénom de mon père ?</option>
														<option>Quel est le nom de ma rue ?</option>
														<option>Quel est le nom de mon employeur ?</option>
														<option>Quel est le nom de mon animal de compagnie ?</option>
													</select>
													<label>Réponse secrète</label>
													<input type="password">
													<div class="actions">
														<span id="supprimer" class="bouton" role="button" tabindex="0">Supprimer</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="modale-resultats" class="conteneur-modale" role="dialog" tabindex="-1">
										<div class="modale" role="document">
											<header>
												<span class="titre">Envoyer les résultats</span>
												<span id="fermerResultats" class="fermer" role="button" tabindex="0">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></svg>
												</span>
											</header>
											<div class="conteneur">
												<div class="contenu">
													<label>Nom ou pseudo</label>
													<input type="text">
													<div class="actions">
														<span id="envoyer" class="bouton" role="button" tabindex="0">Envoyer</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="modale-message" class="conteneur-modale" role="dialog" tabindex="-1">
										<div class="modale" role="document">
											<div class="conteneur">
												<div class="contenu">
													<div class="message"></div>
													<div class="actions">
														<span id="fermerMessage" class="bouton" role="button" tabindex="0">Fermer</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="modale-codeqr" class="conteneur-modale" role="dialog" tabindex="-1">
										<div class="modale" role="document">
											<header>
												<span class="titre">Code QR</span>
												<span id="fermerQR" class="fermer" role="button" tabindex="0">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></svg>
												</span>
											</header>
											<div class="conteneur">
												<div class="contenu">
													<div id="qr"></div>
												</div>
											</div>
										</div>
									</div>
								</main>
								<script src="../../scripts/clipboard.js" type="text/javascript"></script>
								<script src="../../scripts/qrcode.js" type="text/javascript"></script>
								<script src="../../scripts/main.bundle.js" type="text/javascript"></script>
								<script type="text/javascript">
									const resultats = [];
									const resultatsActives = "' . $resultats . '";
									const typeH5P = "' . $json['mainLibrary'] . '";
									const contenusH5P = ["H5P.ArithmeticQuiz", "H5P.Column", "H5P.MultiChoice", "H5P.TrueFalse", "H5P.DragQuestion", "H5P.Crossword", "H5P.Dictation", "H5P.DragText", "H5P.Essay", "H5P.Blanks", "H5P.FindTheWords", "H5P.ImageHotspotQuestion", "H5P.ImageMultipleHotspotQuestion", "H5P.Flashcards", "H5P.MultiMediaChoice", "H5P.ImagePair", "H5P.ImageSequencing", "H5P.MarkTheWords", "H5P.MemoryGame", "H5P.QuestionSet", "H5P.SingleChoiceSet", "H5P.SortParagraphs", "H5P.SpeakTheWords", "H5P.SpeakTheWordsSet", "H5P.Summary", "H5P.AdvancedBlanks", "H5P.InteractiveVideo", "H5P.CoursePresentation", "H5P.InteractiveBook"]
									let lien = "' . $lien . '";
									lien = lien.replace("verifier_token", "enregistrer_resultats");
									const el = document.querySelector("#h5p");
									let customJs = [];
									if (resultatsActives === "true") {
										customJs = ["../../scripts/custom.js"];
									}
									const options = {
										h5pJsonPath: "./h5p",
										frameJs: "../../scripts/frame.bundle.js",
										frameCss: "../../styles/h5p.css",
										librariesPath: "../../libraries",
										frame: false,
										copyright: true,
										embed: false,
										download: false,
										icon: false,
										export: false,
										fullScreen: true,
										reportingIsEnabled: true,
										customCss: ["../../styles/h5p-confirmation-dialog.css", "../../styles/h5p-core-button.css"],
										customJs: customJs
									};
									window.addEventListener("load", function () {
										const h5p = new H5PStandalone.H5P(el, options);
										h5p.then(() => {
											setTimeout(function () {
												document.querySelector("#conteneur-chargement").style.display = "none";
											}, 300);
											if (resultatsActives === "true") {
												H5P.externalDispatcher.on("xAPI", function (event) {
													if (contenusH5P.includes(typeH5P) === true && event.data.statement.hasOwnProperty("object") === true && event.data.statement.hasOwnProperty("result") === true) {
														const contentId = event.data.statement.object.definition.extensions["http://h5p.org/x-api/h5p-local-content-id"];
														let subContentId = "";
														if (event.data.statement.object.definition.extensions.hasOwnProperty("http://h5p.org/x-api/h5p-subContentId") === true) {
															subContentId = event.data.statement.object.definition.extensions["http://h5p.org/x-api/h5p-subContentId"];
														}
														const donneesResultats = definirResultats(event.data.statement.object.definition, event.data.statement.result);
														if (subContentId === "" || (subContentId !== "" && donneesResultats.response !== "")) {
															resultats.push(donneesResultats);
														}
														if ((subContentId === "" && typeH5P !== "H5P.ImageMultipleHotspotQuestion") || (subContentId === "" && typeH5P === "H5P.ImageMultipleHotspotQuestion" && resultats.length === donneesResultats.score.max)) {
															document.querySelector("#modale-resultats").classList.add("ouvert");
														}
													}
												});
											}
										});
									});
									document.querySelector("#partager").addEventListener("click", function (event) {
										if (document.querySelector("#menu-partager").classList.contains("active")) {
											document.querySelector("#menu-partager").classList.remove("active");
										} else {
											document.querySelector("#menu-partager").classList.add("active");
											const largeurBouton = event.target.getBoundingClientRect().width;
											const largeurMenu = document.querySelector("#menu-partager").getBoundingClientRect().width;
											const position = event.target.getBoundingClientRect().left - ((largeurMenu * 90) / 100 - largeurBouton / 2);
											document.querySelector("#menu-partager").style.left = position + "px";
										}
									});
									const clipboard = new ClipboardJS("#copier-lien .lien", {
										container: document.querySelector("#menu-partager"),
										text: function () {
											return "' . $digiquiz . 'q/' . $id . '";
										},
									});
									clipboard.on("success", function () {
										const element = document.createElement("div");
										const id = "notification_" + Date.now().toString(36) + Math.random().toString(36).substr(2);
										element.id = id;
										element.innerHTML = "Lien copié dans le presse-papier.";
										element.classList.add("notification");
										document.body.appendChild(element);
										setTimeout(function () {
											element.parentNode.removeChild(element);
										}, 2000);
									});
									const clipboardIframe = new ClipboardJS("#copier-iframe span", {
										container: document.querySelector("#menu-partager"),
										text: function () {
											return document.querySelector("#copier-iframe input").value;
										},
									});
									clipboardIframe.on("success", function () {
										const element = document.createElement("div");
										const id = "notification_" + Date.now().toString(36) + Math.random().toString(36).substr(2);
										element.id = id;
										element.innerHTML = "Code d&apos;intégration copié dans le presse-papier.";
										element.classList.add("notification");
										document.body.appendChild(element);
										setTimeout(function () {
											element.parentNode.removeChild(element);
										}, 2000);
									});
									new QRCode("qr", {
										text: "' . $digiquiz . 'q/' . $id . '",
										width: 360,
										height: 360,
										colorDark: "#000000",
										colorLight: "#ffffff",
										correctLevel : QRCode.CorrectLevel.H
									});
									document.addEventListener("click", function (event) {
										const partager = document.querySelector("#partager");
										const menuPartager = document.querySelector("#menu-partager");
										if (menuPartager && event.target !== partager && event.target !== menuPartager && !partager.contains(event.target) && !menuPartager.contains(event.target)) {
											document.querySelector("#menu-partager").classList.remove("active");
										}
									});
									document.querySelector("#parametres").addEventListener("click", function (event) {
										document.querySelector("#modale-parametres").classList.add("ouvert");
									});
									document.querySelector("#fermer").addEventListener("click", function () {
										document.querySelector("#modale-parametres").classList.remove("ouvert");
										document.querySelector("#modale-parametres input").value = "";
									});
									document.querySelector("#copier-lien .codeqr").addEventListener("click", function (event) {
										document.querySelector("#menu-partager").classList.remove("active");
										document.querySelector("#modale-codeqr").classList.add("ouvert");
									});
									document.querySelector("#fermerQR").addEventListener("click", function () {
										document.querySelector("#modale-codeqr").classList.remove("ouvert");
									});
									document.querySelector("#fermerResultats").addEventListener("click", function () {
										document.querySelector("#modale-resultats").classList.remove("ouvert");
									});
									document.querySelector("#fermerMessage").addEventListener("click", function () {
										document.querySelector("#modale-message").classList.remove("ouvert");
									});
									document.querySelector("#envoyer").addEventListener("click", function () {
										const nom = document.querySelector("#modale-resultats input").value;
										if (nom !== "") {
											document.querySelector("#conteneur-chargement").style.display = "block";
											const xhr = new XMLHttpRequest();
											xhr.onload = function () {
												setTimeout(function () {
													document.querySelector("#conteneur-chargement").style.display = "none";
												}, 100);
												if (xhr.readyState === xhr.DONE && xhr.status === 200) {
													if (xhr.responseText === "erreur") {
														setTimeout(function () {
															document.querySelector("#modale-message .message").textContent = "Erreur lors de la transmission des résultats.";
															document.querySelector("#modale-message").classList.add("ouvert");
														}, 200);
													} else {
														document.querySelector("#modale-resultats").classList.remove("ouvert");
														setTimeout(function () {
															document.querySelector("#modale-message .message").textContent = "Résultats transmis avec succès.";
															document.querySelector("#modale-message").classList.add("ouvert");
														}, 200);
													}
												} else {
													setTimeout(function () {
														document.querySelector("#modale-message .message").textContent = "Erreur lors de la transmission des résultats.";
														document.querySelector("#modale-message").classList.add("ouvert");
													}, 200);
												}
											};
											xhr.open("POST", lien);
											xhr.setRequestHeader("Content-type", "application/json");
											xhr.send(JSON.stringify({ h5p: "' . $id . '", nom: nom, resultats: resultats }));
										} else {
											document.querySelector("#modale-message .message").textContent = "Veuillez indiquer un nom ou un pseudo pour l\'envoi des résultats.";
											document.querySelector("#modale-message").classList.add("ouvert");
										}
									});
									document.querySelector("#supprimer").addEventListener("click", function () {
										const select = document.querySelector("#modale-parametres select");
										const question = select.options[select.selectedIndex].text
										const reponse = document.querySelector("#modale-parametres input").value;
										if (reponse !== "") {
											document.querySelector("#conteneur-chargement").style.display = "block";
											const xhr = new XMLHttpRequest();
											xhr.onload = function () {
												setTimeout(function () {
													document.querySelector("#conteneur-chargement").style.display = "none";
												}, 100);
												let donnees
												if (xhr.readyState === xhr.DONE && xhr.status === 200) {
													if (xhr.responseText === "erreur") {
														setTimeout(function () {
															document.querySelector("#modale-message .message").textContent = "Erreur lors de la suppression du contenu.";
															document.querySelector("#modale-message").classList.add("ouvert");
														}, 200);
													} else if (xhr.responseText === "non_autorise") {
														setTimeout(function () {
															document.querySelector("#modale-message .message").textContent = "Les informations indiquées ne sont pas correctes.";
															document.querySelector("#modale-message").classList.add("ouvert");
														}, 200);
													} else {
														window.location = "' . $digiquiz . '";
													}
												} else {
													setTimeout(function () {
														document.querySelector("#modale-message .message").textContent = "Erreur lors de la suppression du contenu.";
														document.querySelector("#modale-message").classList.add("ouvert");
													}, 200);
												}
											};
											xhr.open("POST", "../../inc/supprimer.php", true);
											xhr.setRequestHeader("Content-type", "application/json");
											xhr.send(JSON.stringify({ id: "' . $id . '", question: question, reponse: reponse }));
										} else {
											document.querySelector("#modale-message .message").textContent = "Veuillez complétez les champs du formulaire.";
											document.querySelector("#modale-message").classList.add("ouvert");
										}
									});
									function definirResultats (objet, resultat) {
										let name = "";
										if (objet.hasOwnProperty("name") === true) {
											name = objet.name["en-US"];
										} else if (objet.hasOwnProperty("description") === true) {
											name = objet.description["en-US"];
										}
										let interactionType = "";
										if (objet.hasOwnProperty("interactionType") === true) {
											interactionType = objet.interactionType;
										}
										const choices = [];
										if (objet.hasOwnProperty("choices") === true) {
											objet.choices.forEach(function (choice) {
												choices.push({ id: choice.id, description: choice.description["en-US"].replace("\n", "") });
											});
										}
										let correctResponsesPattern = "";
										let caseMatters = "";
										if (objet.hasOwnProperty("correctResponsesPattern") === true) {
											if (objet.correctResponsesPattern[0].includes("{case_matters=false}")) {
												caseMatters = false
											} else if (objet.correctResponsesPattern[0].includes("{case_matters=true}")) {
												caseMatters = true
											}
											correctResponsesPattern = objet.correctResponsesPattern[0].replace("{case_matters=false}", "").replace("{case_matters=true}", "");
										}
										const source = [];
										if (objet.hasOwnProperty("source") === true) {
											objet.source.forEach(function (item) {
												source.push({ id: item.id, description: item.description["en-US"].replace("\n", "") });
											});
										}
										let response = "";
										if (resultat.hasOwnProperty("response") === true) {
											response = resultat.response;
										}
										let score = "";
										if (resultat.hasOwnProperty("score") === true) {
											score = resultat.score;
										}
										let duration = "";
										if (resultat.hasOwnProperty("duration") === true) {
											duration = resultat.duration;
										}
										return { name: name, interactionType: interactionType, choices: choices, correctResponsesPattern: correctResponsesPattern, caseMatters: caseMatters, source: source, response: response, score: score, duration: duration };
									};
									if (window !== window.parent) {
										document.querySelector("header").style.display = "none";
										document.querySelector("#h5p").style.margin = "0 auto";
										document.querySelector("#h5p").style.height = "100%";
									}
								</script>
							</body>
						</html>';
						file_put_contents('../q/' . $id . '/index.html', $html);
						unlink($fichier);
						supprimer($temp);
						echo $id . '|' . $json['mainLibrary'];
					} else {
						echo 'erreur';
					}
				} else {
					echo 'erreur';
				}
			} else {
				echo 'erreur';
			}
			$db = null;
		} else if ($action === 'ajouter' && !empty($_POST['id']) && !empty($_POST['question']) && !empty($_POST['reponse'])) {
			require 'db.php';
			$id = $_POST['id'];
			$question = definirQuestion($_POST['question']);
			$reponse = strtolower($_POST['reponse']);
			$stmt = $db->prepare('SELECT question, reponse FROM digiquiz_contenus WHERE url = :url');
			if ($stmt->execute(array('url' => $id))) {
				$resultat = $stmt->fetchAll();
				if (!$resultat) {
					echo 'contenu_inexistant';
				} else if ($question === $resultat[0]['question'] && password_verify($reponse, $resultat[0]['reponse'])) {
					echo 'contenu_ajoute';
				} else {
					echo 'non_autorise';
				}
			} else {
				echo 'erreur';
			}
			$db = null;
		} else if ($action === 'supprimer' && !empty($_POST['id']) && !empty($_POST['reponse'])) {
			require 'db.php';
			$id = $_POST['id'];
			$reponse = strtolower($_POST['reponse']);
			$stmt = $db->prepare('SELECT reponse FROM digiquiz_contenus WHERE url = :url');
			if ($stmt->execute(array('url' => $id))) {
				$resultat = $stmt->fetchAll();
				if (!$resultat) {
					echo 'contenu_supprime';
				} else if (password_verify($reponse, $resultat[0]['reponse'])) {
					$stmt = $db->prepare('DELETE FROM digiquiz_contenus WHERE url = :url');
					if ($stmt->execute(array('url' => $id))) {
						if (file_exists('../q/' . $id)) {
							supprimer('../q/' . $id);
						}
						echo 'contenu_supprime';
					} else {
						echo 'erreur';
					}
				} else {
					echo 'non_autorise';
				}
			} else {
				echo 'erreur';
			}
			$db = null;
		} else {
			echo 'erreur';
		}
	} else {
		echo 'erreur';
	}
	curl_close($ch);
	exit();
} else {
	echo 'erreur';
	exit();
}

function supprimer ($path) {
	if (is_dir($path) === true) {
		$files = array_diff(scandir($path), array('.', '..'));
		foreach ($files as $file) {
			supprimer(realpath($path) . '/' . $file);
		}
		return rmdir($path);
	} else if (is_file($path) === true) {
		return unlink($path);
	}
	return false;
}

function definirQuestion ($q) {
	$questionSecrete = '';
	switch ($q) {
		case 'motPrefere':
			$questionSecrete = 'Quel est mon mot préféré ?';
			break;
		case 'filmPrefere':
			$questionSecrete = 'Quel est mon film préféré ?';
			break;
		case 'chansonPreferee':
			$questionSecrete = 'Quelle est ma chanson préférée ?';
			break;
		case 'prenomMere':
			$questionSecrete = 'Quel est le prénom de ma mère ?';
			break;
		case 'prenomPere':
			$questionSecrete = 'Quel est le prénom de mon père ?';
			break;
		case 'nomRue':
			$questionSecrete = 'Quel est le nom de ma rue ?';
			break;
		case 'nomEmployeur':
			$questionSecrete = 'Quel est le nom de mon employeur ?';
			break;
		case 'nomAnimal':
			$questionSecrete = 'Quel est le nom de mon animal de compagnie ?';
			break;
	}
	return $questionSecrete;
}

?>
