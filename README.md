# Digiquiz

Digiquiz est une interface simple pour lire des contenus H5P en ligne avec H5P Standalone (https://github.com/tunapanda/h5p-standalone).

Elle est publiée sous licence GNU AGPLv3.
Sauf la fonte HKGrotesk (Sil Open Font Licence 1.1) et H5P Standalone - https://github.com/tunapanda/h5p-standalone - (MIT)

Pour faciliter le déploiement, ce dépôt contient également toutes les librairies H5P - https://github.com/h5p - (MIT)

### Variable d'environnement (fichier .env.production à créer à la racine avant compilation)
Liste des domaines autorisés pour les requêtes POST et l'API, séparés par une virgule (* par défaut)
```
AUTHORIZED_DOMAINS=*
```

### Serveur PHP nécessaire pour générer les contenus
```
php -S 127.0.0.1:8000 (pour le développement uniquement)
```

### Production
Le contenu de ce dépôt peut être déployé directement sur un serveur de fichier.

### Démo
https://ladigitale.dev/digiquiz/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

